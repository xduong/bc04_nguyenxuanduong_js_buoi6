// console.log("duong");

// Bài 1: Tìm số chẵn/lẻ nhỏ hơn 100

document.getElementById("timSoChanLe").onclick = function () {
  var soChan = "";
  var soLe = "";
  for (var i = 0; i < 100; i++) {
    if (i % 2 == 0) {
      soChan = soChan + i + " ";
    } else {
      soLe = soLe + i + " ";
    }
  }
  document.getElementById("ketQuaTimSoChanLe").innerHTML =
    "Số chẵn: " + soChan + `<br>` + "=> Số lẻ: " + soLe;
};

// Bài 2: Đếm số chia hết cho 3 nhỏ hơn 1000

document.getElementById("demSoChiaHetCho3").onclick = function () {
  var count = 0;
  for (var i = 0; i < 1000; i++) {
    if (i % 3 == 0) {
      count++;
    } else {
      continue;
    }
  }
  document.getElementById("soSoChiaHetCho3").innerHTML =
    "Số chia hết cho 3 nhở hơn 1000: " + count + " số";
};

// BÀi 3: Tìm số nguyên dương nhỏ nhất

document.getElementById("timSoNguyenDuong").onclick = function () {
  var sum = 0;
  for (var n = 0; sum < 10000; n++) {
    sum += n;
  }
  document.getElementById("soNguyenDuong").innerHTML =
    "Số nguyên dương nhỏ nhất: " + (n - 1);
};

// Bài 4: Tính tổng

document.getElementById("tinhTong").onclick = function () {
  //   alert(123);
  var giaTriX = document.getElementById("giaTriX").value * 1;
  var giaTriN = document.getElementById("giaTriN").value * 1;

  var tongSn = 0;

  for (var i = 1; i <= giaTriN; i++) {
    tongSn += Math.pow(giaTriX, i);
  }

  document.getElementById("ketQuaTinhTong").innerHTML = "Tổng: " + tongSn;
};

// Bài 5: Tính giai thừa

document.getElementById("tinhGiaiThua").onclick = function () {
  var N = document.getElementById("giaTriN-GiaiThua").value * 1;

  var giaiThua = 1;

  for (var i = 1; i <= N; i++) {
    giaiThua = giaiThua * i;
  }
  document.getElementById("ketQuaTinhGiaiThua").innerHTML =
    "Giai thừa của " + N + " là: " + giaiThua.toLocaleString();
};

//Bài 6: Tạo thẻ div

document.getElementById("taoTheDiv").onclick = function () {
  var contentTaoTheDiv = "";
  for (var i = 1; i <= 10; i++) {
    if (i % 2 == 1) {
      contentTaoTheDiv += `<p class='bg-primary mb-0 p-1'> Div lẻ</p>`;
    } else {
      contentTaoTheDiv += `<p class='bg-danger mb-0 p-1'> Div chẵn</p>`;
    }
  }

  document.getElementById("ketQuaTaoTheDiv").innerHTML = contentTaoTheDiv;
};

// Bài 7: In số nguyên tố

// hàm kiểm tra số nguyên tố
kiemTraSoNguyenTo = function (x) {
  for (var i = 2; i <= Math.sqrt(x); i++) {
    if (x % i == 0) {
      return 0;
    }
  }
  return 1;
};

// console.log("kiemTraSoNguyenTo: ", kiemTraSoNguyenTo(13));

document.getElementById("inSo").onclick = function () {
  //   alert(213);
  var N = document.getElementById("giaTriN-SoNguyenTo").value * 1;

  var cacSoNguyenTo = "";

  for (var i = 2; i <= N; i++) {
    if (kiemTraSoNguyenTo(i) == 1) {
      cacSoNguyenTo += i + " ";
    }
  }
  

  document.getElementById("ketQuaInSo").innerHTML = cacSoNguyenTo;
};
